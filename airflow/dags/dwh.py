from datetime import datetime, timedelta

from airflow import DAG
from airflow.decorators import task_group
from airflow.providers.microsoft.mssql.operators.mssql import MsSqlOperator
from airflow.providers.common.sql.sensors.sql import SqlSensor

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 6, 26),
    'email': ['your@email.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'sql_server_agent_dag',
    default_args=default_args,
    description='Execute SQL Server Agent jobs with Airflow',
    schedule_interval='@daily',
    catchup=False,
)

# Define the SQL Server connection details as Airflow connection variables.

job_name = 'YourAgentJobName'
connection = 'sqlserver'

@task_group()
def async_task():
    call_job = MsSqlOperator(
        task_id='call_job',
        mssql_conn_id='sqlserver',
        sql=f"EXEC msdb.dbo.sp_start_job @job_name = {job_name}",
        dag=dag,
    )

    check_result = SqlSensor(
        task_id='check_result',
        conn_id='sqlserver',
        sql=f"SELECT current_execution_status FROM msdb.dbo.sysjobactivity WHERE job_id IN (SELECT job_id FROM msdb.dbo.sysjobs WHERE name = '{job_name}')",
        poke_interval=60,  # Check every 60 seconds
        timeout=600,  # Timeout after 600 seconds (10 minutes)
        dag=dag,
    )
    call_job >> check_result


sync_task = MsSqlOperator(
    task_id='sync_task',
    mssql_conn_id='sqlserver',
    sql='EXEC your_stored_procedure',
    dag=dag,
)

